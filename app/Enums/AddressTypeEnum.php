<?php

namespace App\Enums;

enum AddressTypeEnum: string
{
    case CASTLE             = 'castle';
    case CITY               = 'city';
    case RESTAURANT         = 'restaurant' ;

    public function label():string {
        return match($this) {
            self::CASTLE                => 'Château',
            self::CITY                  => 'Ville',
            self::RESTAURANT            => 'Restaurant'
        };
    }
}
