<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Address extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'type',
        'resume',
        'latitude',
        'longitude',
        'address_street',
        'address_zip',
        'address_city',
        'address_region',
        'address_country_code',
        'links'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'address_street' => 'array',
//'latitude' => decimal:<precision> ou bien float
        'created_at'    => 'datetime',
        'updated_at'    => 'datetime',
        'deleted_at'    => 'datetime',
        'links'         => 'array'
    ];
}
