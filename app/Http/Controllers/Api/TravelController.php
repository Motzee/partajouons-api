<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\Travel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TravelCreateRequest;
use App\Http\Requests\TravelUpdateRequest;

class TravelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json([
            'data'  => Travel::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TravelCreateRequest $request)
    {
        try {
            $travel = new Travel();
            $travel->title = $request->title;
            $travel->resume = $request->resume;
            $travel->created_by = 1;
            $travel->save();

            return response()->json([
                'status'  => [
                    'code'  => 201,
                    'msg'   => 'Le voyage a bien été ajouté'
                ],
                'data'  => $travel
            ]);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TravelUpdateRequest $request, Travel $travel)
    {
        try {
            $validated = $request->validated();
            $travel->update($validated);
            return response()->json([
                'status'  => [
                    'code'  => 201,
                    'msg'   => 'Le voyage a bien été modifié'
                ],
                'data'  => $travel
            ]);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    /**
     * Soft-Remove the specified resource from storage.
     */
    public function destroy(Travel $travel)
    {
        try {
            if ($travel) {
                $travel->deleted_by = 1 ;
                $travel->save();
                $travel->delete();

                return response()->json([
                    'status'  => [
                        'code'  => 201,
                        'msg'   => 'Le voyage a bien été supprimé'
                    ]
                ]);
            } else {
                return response()->json([
                    'status'  => [
                        'code'  => 404,
                        'msg'   => 'Le voyage demandé n’a pas pu être trouvé'
                    ],
                ]);
            }
        } catch (Exception $e) {
            return response()->json($e);
        }
    }
}
