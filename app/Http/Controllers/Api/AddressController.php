<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressCreateRequest;
use App\Http\Requests\AddressUpdateRequest;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json([
            'data'  => Address::all()
        ]);
    }

    public function index_geojson()
    {
        return response()->json([
            'type'  => 'featureCollection',
            'features'  => [
                [
                    "type"=> "Feature",
                    "geometry"=> [
                        "type"=> "Point",
                        "coordinates"=> [47.55757760, 10.74722550]
                    ],
                    "properties"=> [
                        "prop0"=> "Château de Neuschwanstein"
                    ]
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AddressCreateRequest $request)
    {
        $data = $request->validated();
        //dd($data);
        /*try {*/
            $address = new Address();
            $address->title = $data['title']/*$request->title*/;
            $address->type = $data['type'];
            $address->resume = $data['resume']/*$request->resume*/;
            $address->latitude = $data['latitude']/*$request->latitude*/;
            $address->longitude = $data['longitude']/*$request->longitude*/;
            $address->address_street = $data['address_street']/*$request->address_street*/;
            $address->address_zip = $data['address_zip']/*$request->address_zip*/;
            $address->address_city = $data['address_city']/*$request->address_city*/;
            $address->address_region = $data['address_region'];
            $address->address_country_code = $data['address_country_code']/*$request->address_coutry_code*/;
            $address->links = $data['links'];
            $address->created_by = 1;
            $address->save();

            return response()->json([
                'status'  => [
                    'code'  => 201,
                    'msg'   => 'L\'adresse a bien été ajoutée'
                ],
                'data'  => $address
            ]);
        /*} catch (Exception $e) {
            return response()->json($e);
        }*/
    }

    /**
     * Display the specified resource.
     */
    public function show(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AddressUpdateRequest $request, Address $address)
    {
        try {
            $validated = $request->validated();
            $address->update($validated);
            return response()->json([
                'status'  => [
                    'code'  => 201,
                    'msg'   => 'L\'adresse a bien été modifiée'
                ],
                'data'  => $address
            ]);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Address $address)
    {
        try {
            if ($address) {
                $address->deleted_by = 1 ;
                $address->save();
                $address->delete();

                return response()->json([
                    'status'  => [
                        'code'  => 201,
                        'msg'   => 'L\'adresse a bien été supprimée'
                    ]
                ]);
            } else {
                return response()->json([
                    'status'  => [
                        'code'  => 404,
                        'msg'   => 'L\'adresse demandée n’a pas pu être trouvée'
                    ],
                ]);
            }
        } catch (Exception $e) {
            return response()->json($e);
        }
    }
}
