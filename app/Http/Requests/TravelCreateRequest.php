<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class TravelCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'string|min:2|max:255|required',
            'resume'    => 'string|nullable'
        ];
    }

    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json([
            'success'    => false,
            'msg'    => 'Formulaire non valide',
            'errorsList'    => $validator->errors()
        ]));
    }

    /**
     * translations
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Le titre doit être renseigné',
        ];
    }
}
