<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AddressUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'string|min:2|max:255|required',
            'type'  => 'string|required',
            'resume'    => 'string|nullable',
            'latitude'  => '',
            'longitude'  => '',
            'address_street'  => '',
            'address_zip'  => 'string|max:32',
            'address_city'  => 'string|max:64',
            'address_region'    => 'string|max:255',
            'address_country_code'  => 'string|max:2',
            'links' => ''
        ];
    }
    
    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json([
            'success'    => false,
            'msg'    => 'Formulaire non valide',
            'errorsList'    => $validator->errors()
        ]));
    }

    /**
     * translations
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Le titre doit être renseigné',
        ];
    }
}
