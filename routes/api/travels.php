<?php

use App\Http\Controllers\Api\TravelController;
use Illuminate\Support\Facades\Route;

Route::get('/travels', [TravelController::class, 'index']);
Route::post('/travels/create', [TravelController::class, 'store']);
Route::put('/travels/edit/{travel}', [TravelController::class, 'update']);
Route::delete('/travels/delete/{travel}', [TravelController::class, 'destroy']);