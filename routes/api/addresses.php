<?php

use App\Http\Controllers\Api\AddressController;
use Illuminate\Support\Facades\Route;

Route::get('/addresses', [AddressController::class, 'index']);
Route::get('/addresses/geojson', [AddressController::class, 'index_geojson']);
Route::post('/addresses/create', [AddressController::class, 'store']);
Route::put('/addresses/edit/{address}', [AddressController::class, 'update']);
Route::delete('/addresses/delete/{address}', [AddressController::class, 'destroy']);